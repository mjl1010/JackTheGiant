package clouds;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import java.util.Random;

import collectables.Collectable;
import helpers.GameInfo;
import player.Player;

/**
 * Created by mjl1010 on 14/03/18.
 */

public class CloudsController {
    private World world;
    private Array<Cloud> clouds = new Array<Cloud>();
    private Array<Collectable> collectables = new Array<Collectable>();
    private final float DISTANCE_BETWEEN_CLOUDS = 250f;
    private float minX, maxX;
    private float cameraY;
    private float LAST_CLOUD_POSITION_Y;

    public CloudsController(World world) {
        this.world = world;

        minX = GameInfo.WIDTH / 2f - 110;
        maxX = GameInfo.WIDTH / 2f + 110;

        createClouds();
        positionClouds(true);
    }

    void createClouds() {
        for (int i = 0; i < 2; i++) {
            clouds.add(new Cloud(world, "Dark Cloud"));
        }

        int index = 1;
        for (int i = 0; i < 6; i++) {
            clouds.add(new Cloud(world, "Cloud " + index));
            if (index != 3) index++;
            else index = 1;
        }

        clouds.shuffle();
    }

    public void positionClouds(boolean firstTimeArrainging) {

        while (clouds.get(0).getCloudName().equalsIgnoreCase("Dark Cloud")) clouds.shuffle();

        float positionY = LAST_CLOUD_POSITION_Y;

        if (firstTimeArrainging) positionY = GameInfo.HEIGHT / 2f;

        float tempX;

        for (int i = 0; i<clouds.size; i++) {
            Cloud c = clouds.get(i);

            if (c.getX() == 0 && c.getY() == 0) {
                if (i%2==0) {
                    tempX = randomBetweenNumbers(maxX - 40, minX);
                    c.setDrawLeft(false);
                }
                else {
                    tempX = randomBetweenNumbers(maxX + 40, minX);
                    c.setDrawLeft(true);
                }
                c.setSpritePosition(tempX, positionY);

                positionY -= DISTANCE_BETWEEN_CLOUDS;
                LAST_CLOUD_POSITION_Y = positionY;
            }
        }
    }

    public void drawClouds(SpriteBatch batch) {
        for (Cloud c : clouds) {
            if (c.isDrawLeft()) {
                batch.draw(c, c.getX() - c.getWidth() / 2f - 20, c.getY() - c.getHeight() / 2f);
            } else {
                batch.draw(c, c.getX() - c.getWidth() / 2f + 10, c.getY() - c.getHeight() / 2f);
            }
        }
    }

    public void drawCollectables(SpriteBatch batch) {
        for (Collectable c : collectables){
            batch.draw(c, c.getX(), c.getY());
        }
    }

    public void removeCollectables() {
        for (int i=0; i<collectables.size; i++) {
            if (collectables.get(i).getFixture().getUserData() == "Remove") {
                collectables.get(i).changeFilter();
                collectables.get(i).getTexture().dispose();
                collectables.removeIndex(i);
            }
        }
    }

    private Random random = new Random();

    public void createAndArrangeNewClouds() {
        for (int i = 0; i < clouds.size; i++) {
            if ((clouds.get(i).getY() - GameInfo.HEIGHT / 2 - 15) > cameraY) {
                clouds.get(i).getTexture().dispose();
                clouds.removeIndex(i);
            }
        }

        if (clouds.size == 4) {
            createClouds();
            positionClouds(false);
        }
    }

    public void removeOffScreenCollectables() {
        for (int i=0; i<collectables.size; i++) {
            if (collectables.get(i).getY() - GameInfo.HEIGHT / 2f - 15 > cameraY) {
                collectables.get(i).getTexture().dispose();
                collectables.removeIndex(i);
            }
        }
    }

    public void setCameraY(float y) {
        cameraY = y;
    }

    public Player positionThePlayer(Player player) {
        player = new Player(world, clouds.get(0).getX(), clouds.get(0).getY() + 100);
        return player;
    }

    private float randomBetweenNumbers(float max, float min) {
        return random.nextFloat() * (max - min) + min;
    }
}
